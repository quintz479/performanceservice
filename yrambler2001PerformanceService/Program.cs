﻿using System.ServiceProcess;

namespace yrambler2001PerformanceService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new PerformanceService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
