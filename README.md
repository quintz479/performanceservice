**Require .NET Framework 4**

1. Compile.
2. Install.
3. Change `Configuration.ini`


# Installation 
A simple bat file, located near .exe of service, run as admin:

    cd /d "%~dp0"
    "c:\windows\microsoft.net\framework\v4.0.30319\installutil.exe" /u yrambler2001PerformanceService.exe
    "c:\windows\microsoft.net\framework\v4.0.30319\installutil.exe" yrambler2001PerformanceService.exe
    net start PerformanceService
    pause

# Configuration
After installation, service will create a `Configuration.ini` file in the same directory, where the service is exist.
* `IntervalMsec=6000`- interval that service will wait to search `KillProcesses` in milliseconds.
* `KillProcesses=` - names of processes that will be killed every `IntervalMsec`. if not specified, will do nothing.For Example:
* For example `KillProcesses=software_reporter_tool,CompatTelRunner` - will kill `software_reporter_tool.exe` and `CompatTelRunner.exe`.
* `RestartTelegram=true` - will restart Telegram to tray after PC wakes up from Sleep. ([Issue](https://github.com/telegramdesktop/tdesktop/issues/3640))

After changing configuration file, service must be restarted:

    net stop PerformanceService
    net start PerformanceService

# Uninstallation
A simple bat file, located near .exe of service, run as admin:

    cd /d "%~dp0"
    "c:\windows\microsoft.net\framework\v4.0.30319\installutil.exe" /u yrambler2001PerformanceService.exe
    pause